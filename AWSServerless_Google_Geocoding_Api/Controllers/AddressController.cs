﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AWSServerless_Google_Geocoding_Api.Domain;
using AWSServerless_Google_Geocoding_Api.Domain.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AWSServerless_Google_Geocoding_Api.Controllers
{
    [ApiController]
    public class AddressController : Controller
    {

        private readonly LocationDBContext dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AddressController(LocationDBContext _dbContext, IHttpContextAccessor httpContextAccessor)
        {
            dbContext = _dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        [Authorize]
        [HttpGet]
        [Route("api/address/getmaplist")]
        public IQueryable<Map> GetMapList()
        {
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var uid = Convert.ToInt32(userId);
            //var mapsData = from m in dbContext.Map where m.UserId == uid select m;
            var maps = dbContext.Map.Where(x=>x.UserId == uid).Select(x => new Map()
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                UserId = uid
            });
            return maps;
        }

        [Authorize]
        [HttpGet]
        [Route("api/address/getmaplistbyid/{id}")]
        // for get Map data by UserID  
        public ResponseModel GetMapListById(int UserID)
        {
            ResponseModel _modelObj = new ResponseModel();
            List<Map> maps = GetMapDetails(UserID);
            if (maps.Count > 0)
            {
                _modelObj.Data = maps;
                _modelObj.Status = true;
                _modelObj.Message = "Data listelendi";
            }
            else
            {
                _modelObj.Data = maps;
                _modelObj.Status = false;
                _modelObj.Message = "Data bulunamadı";
            }
            return _modelObj;
        }
        private List<Map> GetMapDetails(int UserID)
        {
            List<Map> maps = null;
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var uid = Convert.ToInt32(userId);
            try
            {
                maps = (from ds in dbContext.Map
                        where ds.UserId == uid
                        select new Map
                        {
                            Id = ds.Id,
                            Latitude = ds.Latitude,
                            Longitude = ds.Longitude,
                            Address = ds.Address,
                            UserId = uid
                        }).ToList();
                return maps;
            }
            catch
            {
                maps = null;

            }
            return maps;
        }
    }
}