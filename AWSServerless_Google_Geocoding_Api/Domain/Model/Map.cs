﻿using System;
using System.Collections.Generic;

namespace AWSServerless_Google_Geocoding_Api.Domain
{
    public partial class Map
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
    }
}
