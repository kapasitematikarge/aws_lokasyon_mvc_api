﻿using AWSServerless_Google_Geocoding_Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AWSServerless_Google_Geocoding_Mvc.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return Redirect("Login");
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult AddUser(string name, string surname, string email, string username, string password)
        {
            LocationDBEntities db = new LocationDBEntities();
            User user = new User();
            user.FirstName = name;
            user.LastName = surname;
            user.Email = email;
            user.Username = username;
            user.Password = password;
            db.User.Add(user);
            db.SaveChanges();
            return Redirect("Login");
        }

        [ValidateAntiForgeryToken]
        public ActionResult Token(User User)
        {
            if (ModelState.IsValid)
            {
                using (LocationDBEntities db = new LocationDBEntities())
                {
                    var obj = db.User.Where(a => a.Username.Equals(User.Username) && a.Password.Equals(User.Password)).FirstOrDefault();

                    if (obj != null)
                    {
                        Session.Add("UserID", obj.UserId.ToString());
                        Session.Add("FirstName", obj.FirstName.ToString() + " " + obj.LastName.ToString());
                        return RedirectToAction("Index", "Admin");
                    }
                }
            }
            return RedirectToAction("Login", "Login");
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            var username = Request.Form["Username"];
            var password = Request.Form["Password"];
            if (Request.Cookies["Username"] != null && Request.Cookies["Password"] != null)
            {
                username = Request.Cookies["Username"].Value;
                password = Request.Cookies["Password"].Value;
            }
            return View();
        }
    }
}