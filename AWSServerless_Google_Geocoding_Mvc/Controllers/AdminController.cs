﻿using AWSServerless_Google_Geocoding_Mvc.Models;
using OfficeOpenXml;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace AWSServerless_Google_Geocoding_Mvc.Controllers
{
    [ControlLogin]
    public class AdminController : Controller
    {
        // GET: Admin
        LocationDBEntities _dbContext = new LocationDBEntities();
        public ActionResult Index()
        {
            ViewBag.Name = Session["FirstName"];
            int uid = Convert.ToInt32(Session["UserID"]);
            var maps = from m in _dbContext.Map where m.UserID == uid select m;
            return View(maps);
        }

        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            LocationDBEntities db = new LocationDBEntities();
            var mapList = new List<AWSServerless_Google_Geocoding_Mvc.Models.Map>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["FileUpload"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var path = Path.Combine(Server.MapPath("~/Content/FileUpload"), file.FileName);
                    file.SaveAs(path);
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {

                            var map = new Map();
                            map.UserID = Convert.ToInt32(Session["UserID"]);
                            map.Latitude = workSheet.Cells[rowIterator, 1].Value.ToString();
                            map.Longitude = workSheet.Cells[rowIterator, 2].Value.ToString();
                            map.Address = workSheet.Cells[rowIterator, 3].Value.ToString();
                            map.ExcelPath = path;
                            map.ModifyUserId = Convert.ToInt32(Session["UserID"]);
                            map.ModifyUser = Session["FirstName"].ToString();
                            map.ModifyDate = DateTime.Now;
                            map.CreateUserId = Convert.ToInt32(Session["UserID"]);
                            map.CreateHost = Session["FirstName"].ToString();
                            map.CreateDate = DateTime.Now;
                            mapList.Add(map);

                        }

                        //ViewBag.Count = mapList.Count;
                        ViewBag.ExcelPath = path;
                    }
                }
            }
            foreach (var item in mapList)
            {
                db.Map.Add(item);
            }
            db.SaveChanges();
            ViewBag.Count = mapList.Count();

            return View(mapList);
        }

        //[HttpPost]
        //public ActionResult UploadFiles2()
        //{
        //    HttpPostedFileBase file = null;
        //    if (Request.Files["FileUpload"] != null)
        //    {
        //        file = Request.Files["FileUpload"];
        //        TempData["haveuploadfile"] = file.FileName;
        //    }
        //    else
        //    {
        //        if (Request.Files["FileUploadd"] != null)
        //        {
        //            file = Request.Files["FileUploadd"];
        //        }
        //    }
        //    string path = Server.MapPath("~/Content/FileUpload");
        //    if (!Directory.Exists(path))
        //    {
        //        Directory.CreateDirectory(path);
        //    }
        //    file.SaveAs(Path.Combine(path, file.FileName));
        //    return RedirectToAction("Index");
        //}


        public JsonResult Listele()
        {
            int userid = Convert.ToInt32(Session["UserID"]);
            var viewModel = _dbContext.Map.Where(x=>x.UserID== userid).Select(x => new {
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Address = x.Address
            }).ToList();
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        //[System.Web.Http.Authorize]
        [System.Web.Mvc.HttpPost]
        public ActionResult InsertMap(string Address, string Latitude, string Longitude)
        {
            Map map = new Map();

            // Insert record
            if (map.Id == 0)
            {
                map.UserID = Convert.ToInt32(Session["UserID"]);
                map.Address = Address;
                map.Latitude = Latitude;
                map.Longitude = Longitude;
                _dbContext.Map.Add(map);
                _dbContext.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                // Update
                var updateRow = _dbContext.Map.Where(x => x.Address == Address).FirstOrDefault();
                updateRow.UserID = Convert.ToInt32(Session["UserID"]);
                updateRow.Address = Address;
                updateRow.Latitude = Latitude;
                updateRow.Longitude = Longitude;
                _dbContext.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
        }

        public ActionResult Liste()
        {
            ViewBag.Name = Session["FirstName"];
            int uid = Convert.ToInt32(Session["UserID"]);
            var maps = from m in _dbContext.Map where m.UserID == uid select m;
            return View(maps);
        }

        //[HttpPost]
        //public ActionResult Upload(FormCollection formCollection)
        //{
        //    LocationDBEntities db = new LocationDBEntities();
        //    var mapList = new List<AWSServerless_Google_Geocoding_Mvc.Models.Map>();
        //    if (Request != null)
        //    {
        //        HttpPostedFileBase file = Request.Files["FileUpload"];
        //        if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
        //        {
        //            string fileName = file.FileName;
        //            string fileContentType = file.ContentType;
        //            byte[] fileBytes = new byte[file.ContentLength];
        //            var data = file.InputStream.Read(fileBytes,0,Convert.ToInt32(file.ContentLength));
        //            using (var package = new ExcelPackage(file.InputStream))
        //            {
        //                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        //                var currentSheet = package.Workbook.Worksheets;
        //                var workSheet = currentSheet.First();
        //                var noOfCol = workSheet.Dimension.End.Column;
        //                var noOfRow = workSheet.Dimension.End.Row;
        //                for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
        //                {

        //                    var map = new Map();
        //                    map.UserID = Convert.ToInt32(Session["UserID"]);
        //                    map.Latitude = workSheet.Cells[rowIterator, 1].Value.ToString();
        //                    map.Longitude = workSheet.Cells[rowIterator, 2].Value.ToString();
        //                    map.Address = workSheet.Cells[rowIterator, 3].Value.ToString();

        //                    mapList.Add(map);

        //                }

        //                ViewBag.Count = mapList.Count;
        //                ViewData["int1"] = mapList.Count;
        //            }
        //        }
        //    }
        //    foreach (var item in mapList)
        //    {
        //        db.Map.Add(item);
        //    }
        //    db.SaveChanges();
        //    return View("Index");
        //}

        public ActionResult Export()
        {
            LocationDBEntities _dbContext = new LocationDBEntities();
            int uid = Convert.ToInt32(Session["UserID"]);
            //var maps = from m in _dbContext.Map where m.UserID == uid orderby m.CreateDate select m;
            var maps = _dbContext.Map.Where(x => x.UserID == uid).OrderByDescending(x => x.CreateDate).Take(50000).ToList();
            Microsoft.Office.Interop.Excel.Application xla = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = xla.Workbooks.Add(XlSheetType.xlWorksheet);
            Worksheet ws = (Worksheet)xla.ActiveSheet;
            ws.Name = "Koordinat Listesi";
            xla.Visible = true;
            ws.Cells[1, 1] = "Enlem";
            ws.Cells[1, 2] = "Boylam";
            ws.Cells[1, 3] = "Adres";
            var i = 2;
            foreach (var item in maps)
            {
                ws.Cells[i, 1] = item.Latitude;
                ws.Cells[i, 2] = item.Longitude;
                ws.Cells[i, 3] = item.Address;
                i++;
            }

            return View("Index");
        }
    }
}